package org.firstinspires.ftc.teamcode;

/**
 * Created by nicolas on 12/27/17 in competition_bot.
 * <p>
 * Copyright (c) ©2017 Nicolas Hohaus
 * Copyright (c) ©2017 Walbots (7175)
 * <p>
 * Resource: https://gitlab.com/roboticsclub/nico/competition_bot
 * Contact: nico@walbots.com, team@walbots.com
 */


/**
 * The class Autonomous_DriveToGlyph is an AutonomousFunction that handles the robot to drive
 * it to a glyph, so that the glyph can be picked up later.
 *
 * For this to succeed the camera of the phone is used and the pictures are analysed with OpenCV.
 * Also the range sensor is used to help identifying the distance from the robot to objects.
 */
public class Autonomous_DriveToGlyph extends AutonomousFunction implements OpenCV_Analysation_Delegate
{
    private static final String TAG = "Auto: Drive_To_Glyph";

    //Variables

    private OpenCV_Thread currentOpenCVThread;      //The current Thread in which OpenCV is running

    private boolean turnRightForSearchingGlyphs;    //Indicates the preferred direction to turn for searching for glyphs

    private boolean isFacingGlyph = false;          //Indicates if the robot is currently facing the glyph
    private float lastMeasuredDistance = -1;        //The last measured distance to the glyph (if available)
    private boolean isAnalysationRunning = false;   //Indicates if an OpenCV analysation is currently in progress
    private long lastAnalysationFinishedTime = 0;   //The last time this class received an analysation result from OpenCV


    //Methods

    /**
     * Initializes an instance of Autonomous_DriveToGlyph
     *
     * @param hardware The hardware controller object of the robot to access and control it's hardware
     * @param name The name of the instance of the AutonomousFunction (will be displayed on the phone)
     * @param turnRightForSearchingGlyphs Indicates the direction the robot should move for searching glyphs
     */
    public Autonomous_DriveToGlyph(BasicHardwareController hardware, String name, boolean turnRightForSearchingGlyphs)
    {
        super(hardware, name);

        this.turnRightForSearchingGlyphs = turnRightForSearchingGlyphs;
    }


    //----------------------------------------------------------------------------------------------
    // AutonomousFunction methods

    /**
     * The setup() method is called by the AutonomousController for setting up the
     * AutonomousFunction preparing it for the run() method
     *
     * In this case the currentOpenCVThread is instantiated
     */
    @Override
    public void setup()
    {
        currentOpenCVThread = new OpenCV_Thread(this);
    }


    /**
     * The run() method is called continually by the AutonomousController until the
     * function returns FINISHED (runs as long as it returns RUNNING)
     *
     * This method will drive the robot to the glyph with help of OpenCV and the range sensor
     *
     * @return RUNNING, FINISHED, (nil=error)
     */
    @Override
    public FunctionState run()
    {

        //Time out so that we are not stuck in this autonomous function forever
        if (getRuntime() > 10000)//TODO: Make timeout reasonable, maybe variable if we already detected a glyph or not (different time outs for different states)
        {
            LoggingController.getCurrentInstance().showLog("DriveToGlyph", "TIMED OUT, Aborting :(");
            return FunctionState.FINISHED;
        }

        //If the robot is facing the glyph, drive the robot to the glyph with help of the range sensor
        //Make sure that the robot is still facing the glyph once a time! (every second)
        if (isFacingGlyph)
        {
            //TODO: Check the range sensor and drive the robot to the glyph

//            if (RANGESENSOR.distance() > 5)
//                //If glyph is too far away drive forward
//                powerDrivingMotors(0.5f, 0.5f, 0.5f, 0.5f);
//            }else
//            {
//                //Robot is directly in front of the glyph, done :))
//                return FunctionState.FINISHED;
//            }


            //Start an OpenCV analysation every second to check if the robot is still facing the glyph
            if (System.currentTimeMillis() - lastAnalysationFinishedTime > 1000 && !isAnalysationRunning)
            {
                currentOpenCVThread.start(); //TODO: Check if start() or run() is the right method to call
                isAnalysationRunning = true;
            }

            return FunctionState.RUNNING;
        }

        //If no OpenCV analysation is running start a new one!
        else if (!isAnalysationRunning)
        {
            currentOpenCVThread.start(); //TODO: Check if start() or run() is the right method to call
            isAnalysationRunning = true;

            return FunctionState.RUNNING;
        }

        //If an OpenCV analysation is running wait for it to finish
        else //if (isAnalysationRunning)
        {
            return FunctionState.RUNNING;
        }
    }

    /**
     * The shutdown() method is called by the AutonomousController for finishing up all tasks and
     * getting to a state where the AutonomousFunction is completed.
     *
     * In this case everything is already finished once this method gets called and nothing is done.
     */
    @Override
    public void shutdown()
    {
        //Ignore this, the robot is located in front of the glyph after the run() method returns FINISHED
        //Nothing to do during shutdown
        return;
    }


    //----------------------------------------------------------------------------------------------
    // OpenCV Thread methods

    /**
     * The openCVAnalysationFinished() method is called by OpenCV_Analysation to submit the
     * results of the current analysation.
     *
     * This method will check the results and turn the robot either to search for glyphs or to
     * directly face a detected glyph. Once the robot is directly facing a glyph, it will set the
     * variable isFacingGlyph to true.
     *
     * @param analysationResult the result of the current analysation
     */
    @Override
    public void openCVAnalysationFinished(OpenCV_Analysation_Result analysationResult)
    {
        //Check results and react (turn the robot so it faces the glyph)
        LoggingController.getCurrentInstance().showLog("OpenCV", "%d glyphs detected, position: %f", analysationResult.numberOfGlyphsDetected, analysationResult.positionOfBestGlyph);

        //Set the time so we know how long it has been since we received the last analysation
        isAnalysationRunning = false;
        lastAnalysationFinishedTime = System.currentTimeMillis();

        //Check if the robot is already facing the best glyph
        if(analysationResult.bestGlyphDetected && analysationResult.positionOfBestGlyph == 0) //TODO: Test out a range that can be accepted for being "facing" the glyph
        {
            isFacingGlyph = true;
            return;
        }else
        {
            isFacingGlyph = false; //If the robot was facing the glyph, but somehow is not anymore
        }

        //Check to which side the robot has to turn to face the best glyph
        if (analysationResult.bestGlyphDetected)
        {
            //TODO: Make tests with the robot of how much it has to turn for specific position numbers until it faces the glyph
            return;
        }

        //If no best glyph is detected turn the robot to the specified direction to search for glyphs
        float leftPower = turnRightForSearchingGlyphs ? 0.5f : -0.5f;
        float rightPower = turnRightForSearchingGlyphs ? -0.5f : 0.5f;

        powerDrivingMotors(leftPower,leftPower,rightPower,rightPower);
        return;
    }

    /**
     * The powerDrivingMotors() will power all four driving motors with their power value.
     *
     * @param backLeft The power value for the back motor on the left side
     * @param frontLeft The power value for the front motor on the left side
     * @param backRight The power value for the back motor on the right side
     * @param frontRight The power value for the front motor on the right side
     */
    private void powerDrivingMotors(float backLeft, float frontLeft, float backRight, float frontRight)
    {
        hardware.powerBackMotorLeft(backLeft);
        hardware.powerBackMotorRight(backRight);
        hardware.powerFrontMotorLeft(frontLeft);
        hardware.powerFrontMotorRight(frontRight);
    }
}
