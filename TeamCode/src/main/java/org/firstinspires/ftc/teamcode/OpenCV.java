package org.firstinspires.ftc.teamcode;

/**
 * Created by nicolas on 12/27/17 in competition_bot.
 * <p>
 * Copyright (c) ©2017 Nicolas Hohaus
 * Copyright (c) ©2017 Walbots (7175)
 * <p>
 * Resource: https://gitlab.com/roboticsclub/nico/competition_bot
 * Contact: nico@walbots.com, team@walbots.com
 */



/**
 * The interface OpenCV_Analysation_Delegate defines how OpenCV communicates with it's callers
 */
interface OpenCV_Analysation_Delegate
{
    /**
     * This method gets called when the current OpenCV analysation is finished
     *
     * @param analysationResult the result of the current analysation
     */
    void openCVAnalysationFinished(OpenCV_Analysation_Result analysationResult);
}


/**
 * The class OpenCV_Analysation_Result contains all information about an OpenCV image analysation
 * that was run by the class OpenCV_Analysation
 */
class OpenCV_Analysation_Result
{
    /**
     * The number of glyphs that were detected and are visible to the robot
     */
    int numberOfGlyphsDetected;

    /**
     * True if there is a glyph detected for which the robot should go
     * (The closest glyph, the best located etc)
     */
    boolean bestGlyphDetected;

    /**
     * If there is a best glyph detected, this will contain it's position (otherwise it is set to 0)
     * The position is defined in the range of -100 until 100
     * -100 is equal to: the glyph is on the very left of what the robot can see
     * +100 is equal to: the glyph is on the very right of what the robot can see
     *    0 is equal to: the robot is directly facing the glyph
     */
    float positionOfBestGlyph;  //Value between -100 and +100 where -100=left, 0=straight, 100=right
}


/**
 * The class OpenCV_Analysation analyses the current image from the camera and calls a delegate when
 * finished
 *
 * In order to analyse the image in a background thread create an instance of OpenCV_Thread and run
 * it (this will run OpenCV_Analysation as well)
 *
 * Otherwise call the run() method to analyse the image in the current thread (not recommended)
 */
class OpenCV_Analysation implements Runnable
{

    /**
     * The delegate property creates a communication way between this class (OpenCV) and the
     * class that is waiting for the results of the analysation
     */
    OpenCV_Analysation_Delegate delegate;

    /**
     * Instantiates a new instance of OpenCV_Analysation with a delegate that can be called once
     * the analysation is finished.
     *
     * @param delegate A class that has a method that can be called when the analysation is finished
     */
    OpenCV_Analysation(OpenCV_Analysation_Delegate delegate)
    {
        this.delegate = delegate;
    }

    /**
     * Runs the analysation of the current image of the camera and calls the delegate once finished
     */
    @Override
    public void run()
    {
        //TODO: Implement OpenCV and use the camera and make a real analysation!!!

        //Get the current picture from the camera

        //Analyze the picture by color

        //Analyze the location of glyphs

        //Find the best glyph

        //Calculate how to move to face the best glyph
        OpenCV_Analysation_Result analyzationResult = new OpenCV_Analysation_Result();
        analyzationResult.numberOfGlyphsDetected = 0;
        analyzationResult.bestGlyphDetected = false;
        analyzationResult.positionOfBestGlyph = 0;

        //Return the analyzed results
        delegate.openCVAnalysationFinished(analyzationResult);
    }
}


/**
 * The class OpenCV_Thread provides everything for running OpenCV_Analysation in the background
 *
 * The thread is set to be high priority!
 */
class OpenCV_Thread extends Thread
{
    /**
     * Instantiates a new thread that runs OpenCV_Analysation in the background as high-priority
     *
     * @param delegate Which will be called once the analysation is finished
     */
    OpenCV_Thread(OpenCV_Analysation_Delegate delegate)
    {
        //Initialize super with a runnable and a name
        super(new OpenCV_Analysation(delegate),"OpenCV_HighPriority_BackgroundThread");

        //Set high-priority (highest is 10)
        this.setPriority(9);
    }
}
