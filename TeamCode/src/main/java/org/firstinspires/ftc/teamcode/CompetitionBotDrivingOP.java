package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

/**
 * Created by walbots on 10/16/17 in competition_bot.
 *
 * Copyright (c) ©2017 Walbots (7175)
 *
 * Resource: https://gitlab.com/roboticsclub/nico/competition_bot
 * Contact: nico@walbots.com, team@walbots.com
 */


/**
 * The class CompetitionBotDrivingOP runs all user controlled actions that are triggered by the keys
 * on two gamepads. To enable a variety of different actions and functionality to the drivers
 * other classes are instantiated and called continually that ensure the the whole functionality
 * is available and the drivers can control the whole robot.
 *
 * @see DrivingController
 * @see ClawController
 * @see KnockOffController
 * @see RelicController
 */
@TeleOp(name="Competition_Bot_Driving", group="Competition")
public class CompetitionBotDrivingOP extends NewOpMode
{
    private DrivingController driver;       //The controller for driving the robot
    private ClawController clawController;  //The controller for the linear slide and the grabber
    private KnockOffController knockOff;    //The controller for the knock off arm and color sensor
    private RelicController relicController;//The controller for the relic recovery arm and grabber

    /**
     * The init() method is part of the OpMode life-cycle and gets called once when the initialize
     * button is pressed on the phone.
     *
     * In this case we will initialize the hardware and the various controllers that will enable
     * the user to control the robot.
     *
     * @param initialize This boolean will always be true, you can ignore it
     * @return Whether the OpMode initialized successfully
     */
    @Override
    public boolean init(boolean initialize)
    {
        driver = new DrivingController(this);
        clawController = new ClawController(this);
        knockOff = new KnockOffController(this);
        relicController = new RelicController(this);

        return true;//Successfully initialized
    }

    /**
     * The loop() method is part of the OpMode life-cycle and gets called repeatedly after the play
     * button is pressed on the phone.
     *
     * In this case all controllers are called so they can read the input from the gamepad and react
     * with various actions the robot can perform.
     *
     * So that we do not have to worry about all possible robot actions in this place, those actions
     * are divided up into separate controllers and their run() methods. With that we only have to
     * worry about specific actions and functionality in their controllers instead of about
     * everything at once here.
     */
    @Override
    public void loop()
    {
        driver.drive();
        clawController.run();
        knockOff.run();
        relicController.run();
    }

}
