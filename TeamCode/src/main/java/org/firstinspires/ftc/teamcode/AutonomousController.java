package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;

/**
 * Created by nicolas on 11/7/17 in competition_bot.
 *
 * Copyright (c) ©2017 Nicolas Hohaus
 * Copyright (c) ©2017 Walbots (7175)
 *
 * Resource: https://gitlab.com/roboticsclub/nico/competition_bot
 * Contact: nico@walbots.com, team@walbots.com
 */


/**
 * The class AutonomousController is designed to control a set of AutonomousFunctions and their
 * execution in a standardized way. A set of actions (AutonomousFunctions) have to be instantiated
 * and configured so that they can be passed to the AutonomousController. Then the autonomous
 * controller takes care about starting and running all these little actions in the right order and
 * in the right way.
 */
public class AutonomousController
{
    private AutonomousFunction[] autonomousFunctions;   //All AutonomousFunctions to run (ordered)
    private OpMode currentAutonomous;       //Current AutonomousMode (OpMode) the user has selected
    private LoggingController loggingController; //for displaying logs on the phone

    /**
     * Instantiates an AutonomousController with an ordered array of all AutonomousFunctions
     * that the controller should iterate through and the current running autonomous mode (OpMode)
     *
     * @param functionArray ordered array of all AutonomousFunctions to run
     * @param autonomousMode the current autonomous mode (OpMode) that is running
     */
    public AutonomousController(AutonomousFunction[] functionArray, OpMode autonomousMode)
    {
        autonomousFunctions = functionArray;
        currentAutonomous = autonomousMode;
        loggingController = LoggingController.getCurrentInstance();
    }

    /**
     * The runAllFunctions() method runs all AutonomousFunctions one by one going through their
     * life-cycle (initialize(), run(), shutdown()). The user is notified by logs on the phone about
     * the current status and the AutonomousFunction that is running.
     *
     * @throws InterruptedException
     */
    public void runAllFunctions() throws InterruptedException
    {
        for (AutonomousFunction autonomousFunction:autonomousFunctions)
        {
            //Initialization
            autonomousFunction.initialize();

            loggingController.showLog(autonomousFunction.name, "Running....");

            //Running
            while (autonomousFunction.run() == AutonomousFunction.FunctionState.RUNNING) {}

            //Waiting
            Thread.sleep(autonomousFunction.timeToWaitAfterFinished);

            //Shutting down
            autonomousFunction.shutdown();

            loggingController.showLog(autonomousFunction.name, "Completed...");
        }
    }
}
