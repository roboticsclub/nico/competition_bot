package org.firstinspires.ftc.teamcode;

/**
 * Created by nicolas on 1/2/18 in competition_bot.
 * <p>
 * Copyright (c) ©2018 Nicolas Hohaus
 * Copyright (c) ©2018 Walbots (7175)
 * <p>
 * Resource: https://gitlab.com/roboticsclub/nico/competition_bot
 * Contact: nico@walbots.com, team@walbots.com
 */


/**
 * The class AutonomousMode_BackgroundThread is running the autonomous mode as a background thread.
 * It is managing the creation and execution of all various actions (AutonomousFunctions) that have
 * to be run during the autonomous period.
 *
 * The autonomous mode will be initialized in the AutonomousMode class.
 */
public class AutonomousMode_BackgroundThread implements Runnable
{
    AutonomousMode currentAutonomousMode;           //The currently running autonomous mode

    private BasicHardwareController hardware;       //For accessing the robot's hardware
    private boolean isBlueAlliance;                 //Indicating in which alliance we are in
    private boolean isFrontPlace;                   //Indicating on which place we are starting

    private boolean isAutonomousFinished = false;   //Indicating if all actions are completed

    /**
     * Initializing an AutonomousThread with the current AutonomousMode
     *
     * @param currentAutonomousMode The currently running autonomous mode
     */
    AutonomousMode_BackgroundThread(AutonomousMode currentAutonomousMode)
    {
        this.currentAutonomousMode = currentAutonomousMode;

        this.hardware = currentAutonomousMode.hardware;
        this.isBlueAlliance = currentAutonomousMode.isBlueAlliance();
        this.isFrontPlace = currentAutonomousMode.isFrontPlace();
    }

    /**
     * The run() method is part of the Thread life-cycle and gets called after the Thread was
     * started. Everything should already be initialized for running this autonomous mode in the
     * class AutonomousMode!
     *
     * In this case the run() method only runs once and will be directly returned afterwards. All
     * actions as AutonomousFunctions are instantiated and specified so that the
     * AutonomousController runs through them step by step completing all actions we want to do
     * during autonomous.
     */
    @Override
    public final void run()
    {
        //Check if the autonomous mode is done, if yes return
        if (isAutonomousFinished)
        {
            return;
        }


        //Run autonomous functions


        AutonomousFunction[] functions = new AutonomousFunction[2];

        //Knock Off Down
        functions[0] = new Autonomous_KnockOffArm(hardware,"Move Knock Off Arm Down", true);
        functions[0].timeToWaitAfterFinished = 1000;

        //Color sensor
        functions[1] = new Autonomous_ColorSensor(hardware, "Sense Color of Balls");
        functions[1].timeToWaitAfterFinished = 1000;

        //Run functions
        runFunctions(functions);

        //Check out color
        boolean isBlueAtTheRightSide = ((Autonomous_ColorSensor)functions[1]).resultIsBlue;


        if (isBlueAtTheRightSide)
        {
            LoggingController.getCurrentInstance().showLog("Color detected", "Detected Blue");
        }else
        {
            LoggingController.getCurrentInstance().showLog("Color detected", "Detected Red");
        }

        //reset functions
        functions = new AutonomousFunction[3];

        //Knock off the ball (or turn back)
        float[] drivingPower = new float[5];
        drivingPower[0] = 0.3f;
        drivingPower[1] = -0.3f;
        drivingPower[2] = 0.3f;
        drivingPower[3] = -0.3f;
        functions[isBlueAtTheRightSide == !isBlueAlliance ? 2 : 0] = new Autonomous_Driving(hardware,"Curving", drivingPower);

        //Turn back (or knock off the ball)
        float[] drivingPower2 = new float[4];
        drivingPower2[0] = -0.3f;
        drivingPower2[1] = 0.3f;
        drivingPower2[2] = -0.3f;
        drivingPower2[3] = 0.3f;
        functions[isBlueAtTheRightSide == !isBlueAlliance ? 0 : 2] = new Autonomous_Driving(hardware,"Curving", drivingPower2);

        functions[0].timeToWaitAfterFinished = 300;
        functions[2].timeToWaitAfterFinished = 300;

        //Knock Off Up
        functions[1] = new Autonomous_KnockOffArm(hardware,"Move Knock Off Arm Up", false);
        functions[1].timeToWaitAfterFinished = 1000;


        //Run functions
        runFunctions(functions);

        //Park Robot in Save Zone
        driveIntoSaveZone();

        isAutonomousFinished = true;
    }

    /**
     * The driveIntoSaveZone() method manages that the right method gets called so that the robot
     * always drives into the save zone independent to the starting position.
     */
    private void driveIntoSaveZone()
    {
        if (isFrontPlace)
        {
            driveIntoSaveZone_FrontPlace();
        }else
        {
            driveIntoSaveZone_BackPlace();
        }
    }

    /**
     * The driveIntoSaveZone_FrontPlace() method manages that the robot drives into the save zone
     * when the starting position was in the front.
     */
    private void driveIntoSaveZone_FrontPlace()
    {
        //TODO: Develop autonomous for the front places to park in the save zone
    }

    /**
     * The driveIntoSaveZone_BackPlace() method manages that the robot drives into the save zone
     * when the starting position was in the back.
     */
    private void driveIntoSaveZone_BackPlace()
    {
        AutonomousFunction[] functions = new AutonomousFunction[3];

        //Drive off balancing board
        float[] power = new float[4];
        power[0] = -0.5f;
        power[1] = -0.5f;
        power[2] = -0.5f;
        power[3] = -0.5f;
        functions[0] = new Autonomous_Driving(hardware,"Drive Forward", power);
        functions[0].timeToWaitAfterFinished = 1000;

        //Turning around to face the save zone
        power = new float[4];
        power[0] = isBlueAlliance ? -0.5f : 0.5f;
        power[1] = isBlueAlliance ? 0.5f : -0.5f;
        power[2] = isBlueAlliance ? -0.5f : 0.5f;
        power[3] = isBlueAlliance ? 0.5f : -0.5f;
        functions[1] = new Autonomous_Driving(hardware,"Curve", power);
        functions[1].timeToWaitAfterFinished = 1000;

        //Drive into save zone
        power = new float[4];
        power[0] = -0.5f;
        power[1] = -0.5f;
        power[2] = -0.5f;
        power[3] = -0.5f;
        functions[2] = new Autonomous_Driving(hardware,"Drive Forward", power);
        functions[2].timeToWaitAfterFinished = 1000;

        //Run functions
        runFunctions(functions);
    }

    /**
     * The runFunctions() method creates an AutonomousController that runs the given
     * AutonomousFunctions and takes care about their execution step by step
     */
    private void runFunctions(AutonomousFunction[] functions)
    {
        AutonomousController controller = new AutonomousController(functions, currentAutonomousMode);
        try {
            controller.runAllFunctions();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
