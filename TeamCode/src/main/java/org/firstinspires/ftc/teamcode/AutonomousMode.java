package org.firstinspires.ftc.teamcode;

/**
 * Created by nicolas on 12/2/17 in competition_bot.
 *
 * Copyright (c) ©2017 Nicolas Hohaus
 * Copyright (c) ©2017 Walbots (7175)
 *
 * Resource: https://gitlab.com/roboticsclub/nico/competition_bot
 * Contact: nico@walbots.com, team@walbots.com
 */



/**
 * The abstract class AutonomousMode runs all varieties of autonomous modes. Since there are only a
 * few differences between the different autonomous modes, a few abstract methods are defined that
 * return for example the place of an autonomous mode so that this class can run the right actions.
 * So the different autonomous modes that implement (extend) this class only have to return the
 * correct values for those abstract methods and this class will handle it.
 *
 * (The autonomous mode will run as a background thread)
 * @see AutonomousMode_BackgroundThread
 */
public abstract class AutonomousMode extends NewOpMode
{
    //----------------------------------------------------------------------------------------------
    // Properties
    private Thread autonomousBackgroundThread;  //The currently running autonomous thread

    //----------------------------------------------------------------------------------------------
    // Abstract methods

    /**
     * The abstract method isBlueAlliance has to be implemented by classes that extend
     * AutonomousMode so that we know which actions we have to do in specific ways during autonomous.
     *
     * @return Whether it is running for the blue alliance (true) or the red (false)
     */
    public abstract boolean isBlueAlliance();

    /**
     * The abstract method isFrontPlace has to be implemented by classes that extend
     * AutonomousMode so that we know which actions we have to do in specific ways during autonomous.
     *
     * @return Whether it is running in the front place (true) or the back place (false)
     */
    public abstract boolean isFrontPlace();



    //Autonomous life cycle (init, loop) and helper methods

    //----------------------------------------------------------------------------------------------
    // Initialization

    /**
     * The init() method is part of the OpMode life-cycle and gets called once when the initialize
     * button is pressed on the phone.
     *
     * In this case we will initialize the autonomous mode making it ready for running.
     *
     * @param initialize This boolean will always be true, you can ignore it
     * @return Whether the OpMode initialized successfully
     */
    @Override
    public boolean init(boolean initialize)
    {
        hardware.initializeKnockOffArmServo();
        hardware.initialiseTopGrabberServos();
        hardware.initialiseBottomGrabberServos();

        return true;
    }

    /**
     * The start() method is part of the OpMode life-cycle and gets called once after the start
     * button is pressed on the phone.
     *
     * In this case the start() method will start a background thread running the autonomous mode.
     * For details see the AutonomousMode_BackgroundThread class.
     */
    @Override
    public void start()
    {
        Runnable autonomousRunnable = new AutonomousMode_BackgroundThread(this);

        autonomousBackgroundThread = new Thread(autonomousRunnable, "Autonomous_Background");
        autonomousBackgroundThread.start();
    }

    /**
     * The loop() method is part of the OpMode life-cycle and gets called continuously after the
     * start button was pressed on the phone.
     *
     * This function does not do anything in this implementation.
     */
    @Override
    public void loop()
    {
        //Ignore this, we are not doing anything in the loop() method of the OpMode life-cycle
    }

    /**
     * The stop() method is part of the OpMode life-cycle and gets called once after the stop button
     * is pressed on the phone.
     *
     * In this case the stop() method will stop the currently in the background running autonomous
     * thread.
     */
    @Override
    public void stop()
    {
        super.stop();
        autonomousBackgroundThread.interrupt();
    }
}
