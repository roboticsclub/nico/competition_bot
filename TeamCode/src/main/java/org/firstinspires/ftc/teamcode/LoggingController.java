package org.firstinspires.ftc.teamcode;

import android.util.Log;

import org.firstinspires.ftc.robotcore.external.Telemetry;

/**
 * Created by nicolas on 12/30/17 in competition_bot.
 * <p>
 * Copyright (c) ©2017 Nicolas Hohaus
 * Copyright (c) ©2017 Walbots (7175)
 * <p>
 * Resource: https://gitlab.com/roboticsclub/nico/competition_bot
 * Contact: nico@walbots.com, team@walbots.com
 */


/**
 * The class LoggingController is designed for logging. All logs will be displayed on the phone
 * in a good way so that the robot drivers can always see the most actual messages without even
 * touching the phone.
 *
 * After a LoggingController is initialized by an OpMode with it's telemetry, the current
 * LoggingController can be accessed from everywhere in the code through the static method
 * LoggingController.getCurrentInstance()
 *
 * New Logs can be shown through the method showLog(). For example:
 *
 *      LoggingController.getCurrentInstance().showLog("Title", "This is a log");
 *
 * If no LoggingController was initialized by an OpMode, the method getCurrentInstance() will return
 * null and a NullPointerException might be thrown by trying to access any methods.
 */
public class LoggingController
{
    private final static String TAG = "Logging Controller"; //Private TAG string used for logging


    //----------------------------------------------------------------------------------------------
    // Static properties and methods

    private static LoggingController currentInstance;   //The current instance of LoggingController

    /**
     * The getCurrentInstance() method searches for the last instantiated LoggingController and if
     * available returns it. Otherwise null will be returned.
     *
     * @return current instance of LoggingController or null
     */
    public static LoggingController getCurrentInstance()
    {
        if(currentInstance == null) //If no instance is available log an error
        {
            Log.e(TAG, "getCurrentInstance: No instance available! App may crash in future." +
                    " To prevent this create an instance of LoggingController in your OpMode" +
                    " before calling the getCurrentInstance() method.");
        }

        return currentInstance;
    }


    //----------------------------------------------------------------------------------------------
    // Properties

    private Telemetry currentTelemetry; //Current telemetry used for showing logs on the phone

    private String[] logs = {};         //Array of logs (new logs first)


    //----------------------------------------------------------------------------------------------
    // Methods

    /**
     * Instantiates a new LoggingController and saves it as the CurrentLogging controller so that
     * it can be accessed from everywhere over the getCurrentInstance() method.
     *
     * @param currentTelemetry current telemetry object used for showing logs on the phone
     */
    LoggingController(Telemetry currentTelemetry)
    {
        this.currentTelemetry = currentTelemetry;

        currentInstance = this;
    }

    /**
     * The shoLog() method adds another row of log message to the top of the shown logs on the
     * phone. The logs will be displayed in the format "TITLE: MESSAGE"
     *
     * @param title Title for the log message
     * @param msg Log message
     */
    void showLog(String title, String msg)
    {
        Log.d(TAG, "Title: " + title + " Message: " + msg);

        //Create new array to save logs in
        String[] newLogsArray = new String[logs.length + 2];

        //Save the new log at the beginning so it will be displayed at the top (first)
        newLogsArray[0] = title;
        newLogsArray[1] = msg;

        //Add all old logs
        for (int i = 2; i < newLogsArray.length; i++)
        {
            newLogsArray[i] = logs[i - 2];
        }

        //Update the logs array
        logs = newLogsArray;

        //Show the updated logs on the phone
        updateTelemetry();
    }

    /**
     * The shoLog() method adds another row of log message to the top of the shown logs on the
     * phone. The logs will be displayed in the format "TITLE: MESSAGE"
     *
     * @param title Title for the log message
     * @param format Log message string format
     * @param args Log message arguments
     */
    void showLog(String title, String format, Object...args)
    {
        showLog(title, String.format(format,args));
    }

    /**
     * The updateTelemetry() method shows all logs that have been logged by the LoggingController
     * on the phone.
     */
    private void updateTelemetry()
    {
        //Clear all logs
        currentTelemetry.clearAll();

        //Add all logs so they will be displayed
        for (int i = 0; i < logs.length; i += 2)
        {
            currentTelemetry.addData(logs[i], logs[i + 1]);
        }

        //Show logs on the phone
        currentTelemetry.update();
    }
}