package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.Servo;

/**
 * Created by nicolas on 10/16/17 in competition_bot.
 *
 * Copyright (c) ©2017 Nicolas Hohaus
 * Copyright (c) ©2017 Walbots (7175)
 *
 * Resource: https://gitlab.com/roboticsclub/nico/competition_bot
 * Contact: nico@walbots.com, team@walbots.com
 */


/**
 * The class BasicHardwareController is controlling the complete hardware of the competition robot
 * from 2017/18. The hardware properties are set to be private enforcing to use the defined methods
 * to set/get properties of the hardware. On this basic hardware level we control custom
 * specialities of different kind of hardware so that we do not have to take care about those in
 * other places in the code.
 *
 * For example: We enable motors to turn in reverse by having a negative power set. We care about
 * the motor turning FORWARD or BACKWARD in this controller, so that we can just set a positive or
 * negative power somewhere else and don't have to worry about all specialities everywhere in the
 * code.
 */
public class BasicHardwareController
{
    //----------------------------------------------------------------------------------------------
    // Private Properties
    private HardwareMap hardwareMap;        //Hardware map for accessing the current hardware

    //----------------------------------------------------------------------------------------------
    // Hardware Input

    private ColorSensor knockOffColorSensor;    //Color sensor mounted on the knock off arm


    //----------------------------------------------------------------------------------------------
    // Hardware Output

    private DcMotor frontMotorLeft;         //The motor moving the wheel on the front,left
    private DcMotor frontMotorRight;        //The motor moving the wheel on the front,right
    private DcMotor backMotorLeft;          //The motor moving the wheel on the back,left
    private DcMotor backMotorRight;         //The motor moving the wheel on the back,right

    private DcMotor linearSlideMotorTop;    //Top motor moving the linear slide (with grabbers)
    private DcMotor linearSlideMotorBottom; //Bottom motor moving the linear slide (with grabbers)
    private Servo grabberTopServoLeft;      //The servo moving the left top grabber plane
    private Servo grabberTopServoRight;     //The servo moving the right top grabber plane
    private Servo grabberBottomServoLeft;   //The servo moving the left bottom grabber plane
    private Servo grabberBottomServoRight;  //The servo moving the right bottom grabber plane

    private Servo knockOffArmServo;         //The servo moving the knock off arm up/down

    private DcMotor relicRecoveryArmMotor;  //The motor moving the relic arm in and out
    private Servo relicRecoveryGrabberServo;//The servo moving the relic grabber (open/close)


    //----------------------------------------------------------------------------------------------
    // Initialisation

    /**
     * Initializes a BasicHardwareController with the HardwareMap from the running OpMode.
     * This class provides functionality to access and control the hardware of the 2017/18
     * competition robot we have build.
     *
     * @param hardwareMap The HardwareMap of the currently running OpMode
     */
    public BasicHardwareController(HardwareMap hardwareMap)
    {
        this.hardwareMap = hardwareMap;

        //Input
        knockOffColorSensor = (ColorSensor) getHardware(ColorSensor.class,"knockOffColorSensor");

        //Output
        frontMotorLeft = getDcMotor("frontMotorLeft");
        frontMotorRight = getDcMotor("frontMotorRight");
        backMotorLeft = getDcMotor("backMotorLeft");
        backMotorRight = getDcMotor("backMotorRight");

        linearSlideMotorTop = getDcMotor("linearSlideMotorTop");
        linearSlideMotorBottom = getDcMotor("linearSlideMotorBottom");
        grabberTopServoLeft = getServo("grabberTopServoLeft");
        grabberTopServoRight = getServo("grabberTopServoRight");
        grabberBottomServoLeft = getServo("grabberBottomServoLeft");
        grabberBottomServoRight = getServo("grabberBottomServoRight");

        knockOffArmServo = getServo("knockOffArmServo");

        relicRecoveryArmMotor = getDcMotor("relicRecoveryArmMotor");
        relicRecoveryGrabberServo = getServo("relicRecoveryGrabberServo");

        checkIfAllHardwareIsAvailable();
    }


    //----------------------------------------------------------------------------------------------
    // Sensors (Color, Range etc)

    /**
     * Turning the LED of the color sensor that is mounted on the knock off arm on or off
     *
     * @param turnOn Indicates if the LED of the ColorSensor should be turned on(true) or off(false)
     */
    void enableKnockOffColorSensorLED(boolean turnOn)
    {
        if(!isAvailable(knockOffColorSensor))//Just return if sensor not available
        {
            return;
        }

        knockOffColorSensor.enableLed(turnOn);
    }

    /**
     * Reading the current color from the color sensor that is mounted on the knock off arm and
     * determining the value of blue.
     *
     * @return The blue value that the color sensor senses
     */
    int getKnockOffColorSensorBlue()
    {
        if(!isAvailable(knockOffColorSensor))//Just return if sensor not available
        {
            return -1;  //Error number
        }

        int blue =  knockOffColorSensor.blue();

        return blue;
    }

    /**
     * Reading the current color from the color sensor that is mounted on the knock off arm and
     * determining the value of red.
     *
     * @return The red value that the color sensor senses
     */
    int getKnockOffColorSensorRed()
    {
        if(!isAvailable(knockOffColorSensor))//Just return if sensor not available
        {
            return -1;  //Error number
        }

        int red = knockOffColorSensor.red();

        return red;
    }


    //----------------------------------------------------------------------------------------------
    // Motors and Servos

    /**
     * Setting power on the motor that moves the front wheel on the left side.
     *
     * @param power Power value between -1 and 1 moving the motor (0=stop)
     */
    void powerFrontMotorLeft(float power)
    {
        powerMotor(frontMotorLeft, -power);
    }

    /**
     * Setting power on the motor that moves the front wheel on the right side.
     *
     * @param power Power value between -1 and 1 moving the motor (0=stop)
     */
    void powerFrontMotorRight(float power)
    {
        powerMotor(frontMotorRight, power);
    }

    /**
     * Setting power on the motor that moves the back wheel on the left side.
     *
     * @param power Power value between -1 and 1 moving the motor (0=stop)
     */
    void powerBackMotorLeft(float power)
    {
        powerMotor(backMotorLeft, -power);
    }

    /**
     * Setting power on the motor that moves the back wheel on the right side.
     *
     * @param power Power value between -1 and 1 moving the motor (0=stop)
     */
    void powerBackMotorRight(float power)
    {
        powerMotor(backMotorRight, power);
    }

    /**
     * Setting power on the motors that move the linear slide up and down on which the grabbers are
     * mounted.
     *
     * Currently we need two motors working together in order to bring up enough force to lift the
     * linear slide, this is handled in this method.
     *
     * @param power Power value between -1 and 1 moving the motors (0=stop)
     */
    void powerLinearSlideMotors(float power)
    {
        powerMotor(linearSlideMotorTop, power);
        powerMotor(linearSlideMotorBottom, -power);
    }

    /**
     * Setting power on the servo that moves the plane at the left side of the top grabber.
     *
     * The power value will be translated into a servo position, and in order to keep the servo
     * moving this method has to be continually called. If the power is set to 1, the servo will be
     * moved by 1 degree for each time this method is called. The power value can be out of the
     * range from -1 until +1, but once the servo reaches it's maximum position it will stop moving.
     *
     * @param power Power value moving the servo (0=stop)
     */
    void powerTopGrabberServoLeft(double power)
    {
        powerServo(grabberTopServoLeft, power);
    }

    /**
     * Setting power on the servo that moves the plane at the right side of the bottom grabber.
     *
     * The power value will be translated into a servo position, and in order to keep the servo
     * moving this method has to be continually called. If the power is set to 1, the servo will be
     * moved by 1 degree for each time this method is called. The power value can be out of the
     * range from -1 until +1, but once the servo reaches it's maximum position it will stop moving.
     *
     * @param power Power value moving the servo (0=stop)
     */
    void powerBottomGrabberServoRight(double power)
    {
        powerServo(grabberBottomServoRight, power);
    }

    /**
     * Setting power on the servo that moves the plane at the left side of the bottom grabber.
     *
     * The power value will be translated into a servo position, and in order to keep the servo
     * moving this method has to be continually called. If the power is set to 1, the servo will be
     * moved by 1 degree for each time this method is called. The power value can be out of the
     * range from -1 until +1, but once the servo reaches it's maximum position it will stop moving.
     *
     * @param power Power value moving the servo (0=stop)
     */
    void powerBottomGrabberServoLeft(double power)
    {
        powerServo(grabberBottomServoLeft, power);
    }

    /**
     * Setting power on the servo that moves the plane at the right side of the top grabber.
     *
     * The power value will be translated into a servo position, and in order to keep the servo
     * moving this method has to be continually called. If the power is set to 1, the servo will be
     * moved by 1 degree for each time this method is called. The power value can be out of the
     * range from -1 until +1, but once the servo reaches it's maximum position it will stop moving.
     *
     * @param power Power value moving the servo (0=stop)
     */
    void powerTopGrabberServoRight(double power)
    {
        powerServo(grabberTopServoRight, power);
    }

    /**
     * Setting power on the servo that moves the knock off arm
     *
     * The power value will be translated into a servo position, and in order to keep the servo
     * moving this method has to be continually called. If the power is set to 1, the servo will be
     * moved by 1 degree for each time this method is called. The power value can be out of the
     * range from -1 until +1, but once the servo reaches it's maximum position it will stop moving.
     *
     * @param power Power value moving the servo (0=stop)
     */
    void powerKnockOffArmServo(double power)
    {
        powerServo(knockOffArmServo, power);
    }

    /**
     * Setting power on the motor that moves the relic recovery arm in and out.
     *
     * @param power Power value between -1 and 1 moving the motor (0=stop)
     */
    void powerRelicRecoveryArmMotor(float power)
    {
        powerMotor(relicRecoveryArmMotor, power);
    }

    /**
     * Setting power on the servo that moves (open/closes) the relic recovery grabber.
     *
     * @param power Power value moving the servo (0=stop)
     */
    void powerRelicRecoveryGrabberServo(double power)
    {
        powerServo(relicRecoveryGrabberServo, power);
    }

    /**
     * Setting default values to the servos that move the planes of the top grabber.
     * The servos will be moved to its starting positions so they are prepared for future use.
     */
    void initialiseTopGrabberServos()
    {
        if(!isAvailable(grabberTopServoLeft) || !isAvailable(grabberTopServoRight))
        {//Just return if servos not available
            return;
        }

        //Initialize left grabber
        grabberTopServoLeft.setDirection(Servo.Direction.REVERSE);
        grabberTopServoLeft.setPosition(grabberTopServoLeft.MAX_POSITION);

        //Initialized right grabber
        grabberTopServoRight.setDirection(Servo.Direction.FORWARD);
        grabberTopServoRight.setPosition(grabberTopServoRight.MAX_POSITION);
    }

    /**
     * Setting default values to the servos that move the planes of the bottom grabber.
     * The servos will be moved to its starting positions so they are prepared for future use.
     */
    void initialiseBottomGrabberServos()
    {
        if(!isAvailable(grabberBottomServoLeft) || !isAvailable(grabberBottomServoRight))
        {//Just return if servos not available
            return;
        }

        //Initialize left grabber
        grabberBottomServoLeft.setDirection(Servo.Direction.REVERSE);
        grabberBottomServoLeft.setPosition(grabberBottomServoLeft.MAX_POSITION);

        //Initialized right grabber
        grabberBottomServoRight.setDirection(Servo.Direction.FORWARD);
        grabberBottomServoRight.setPosition(grabberBottomServoRight.MAX_POSITION);
    }

    /**
     * Setting default values to the servo that moves the knock off arm.
     * The servo will be moved to its starting position so it is prepared for future use.
     */
    void initializeKnockOffArmServo()
    {
        if(!isAvailable(knockOffArmServo))  //Just return if servo not available
        {
            return;
        }

        knockOffArmServo.setDirection(Servo.Direction.FORWARD);
        knockOffArmServo.setPosition(knockOffArmServo.MAX_POSITION - 0.2); //Initialize servo in an upright position (max - 0.2)
    }

    /**
     * Setting default values to the servo that moves the relic recovery grabber servo.
     * The servo will be moved to its starting position so it is prepared for future use.
     */
    void initializeRelicRecoveryGrabberServo()
    {
        if(!isAvailable(relicRecoveryGrabberServo)) //Just return if servo not available
        {
            return;
        }

        relicRecoveryGrabberServo.setDirection(Servo.Direction.FORWARD);
        relicRecoveryGrabberServo.setPosition(0.5); //Initialize servo in an a little but opened position
    }

    //----------------------------------------------------------------------------------------------
    // Helper functions

    /**
     * The getDcMotor() method looks for a DcMotor with the given name and returns it if available.
     *
     * @param name The DcMotor's name defined in the robot configuration on the user phone
     * @return If available the DcMotor object
     */
    private DcMotor getDcMotor(String name)
    {
        return (DcMotor) getHardware(DcMotor.class, name);
    }

    /**
     * The getServo() method looks for a Servo with the given name and returns it if available.
     *
     * @param name The Servo's name defined in the robot configuration on the user phone
     * @return If available the Servo object
     */
    private Servo getServo(String name)
    {
        return (Servo) getHardware(Servo.class, name);
    }

    /**
     * The getHardware() method looks for the specified hardware and returns it if available.
     *
     * @param hardwareType The type of hardware to look for
     * @param name The hardware's name defined in the robot configuration on the user phone
     * @return If available the hardware object
     */
    private Object getHardware(Class hardwareType, String name)
    {
        Object hardware = null;
        try
        {
            hardware = hardwareMap.get(hardwareType, name);
        }catch (NullPointerException e)
        {
            LoggingController.getCurrentInstance().showLog("Hardware", "Null Pointer Exception");
        }

        return hardware;
    }

    /**
     * The isAvailable() method checks if a given hardware (or object) is currently available.
     *
     * @param hardware Hardware to check it's availability
     * @return Whether the hardware is available or not
     */
    private boolean isAvailable(Object hardware)
    {
        return hardware != null;
    }

    /**
     * The checkIfAllHardwareIsAvailable() method checks the hardware and logs an error if something
     * is not available.
     *
     * @return True if the hardware is available, false if some hardware is not available
     */
    boolean checkIfAllHardwareIsAvailable()
    {
        boolean allAvailable = true;
        LoggingController log = LoggingController.getCurrentInstance();

        //Sensors
        if (!isAvailable(knockOffColorSensor))
        {
            allAvailable = false;
            log.showLog("Hardware", "Color Sensor NOT available!!!");
        }

        //Motors and Servos
        if(!isAvailable(frontMotorLeft))
        {
            allAvailable = false;
            log.showLog("Hardware", "Driving motor front,left NOT available!!!");
        }

        if(!isAvailable(frontMotorRight))
        {
            allAvailable = false;
            log.showLog("Hardware", "Driving motor front,right NOT available!!!");
        }

        if(!isAvailable(backMotorLeft))
        {
            allAvailable = false;
            log.showLog("Hardware", "Driving motor back,left NOT available!!!");
        }

        if(!isAvailable(backMotorRight))
        {
            allAvailable = false;
            log.showLog("Hardware", "Driving motor back,right NOT available!!!");
        }

        if(!isAvailable(linearSlideMotorTop))
        {
            allAvailable = false;
            log.showLog("Hardware", "Top linear slide motor NOT available!!!");
        }

        if(!isAvailable(linearSlideMotorBottom))
        {
            allAvailable = false;
            log.showLog("Hardware", "Bottom linear slide motor NOT available!!!");
        }

        if(!isAvailable(grabberTopServoLeft))
        {
            allAvailable = false;
            log.showLog("Hardware", "Grabber top,left motor NOT available!!!");
        }

        if(!isAvailable(grabberTopServoRight))
        {
            allAvailable = false;
            log.showLog("Hardware", "Grabber top,right motor NOT available!!!");
        }

        if(!isAvailable(grabberBottomServoLeft))
        {
            allAvailable = false;
            log.showLog("Hardware", "Grabber bottom,left motor NOT available!!!");
        }

        if(!isAvailable(grabberBottomServoRight))
        {
            allAvailable = false;
            log.showLog("Hardware", "Grabber bottom,right motor NOT available!!!");
        }

        if(!isAvailable(knockOffArmServo))
        {
            allAvailable = false;
            log.showLog("Hardware", "Knock off arm servo NOT available!!!");
        }

        if(!isAvailable(relicRecoveryArmMotor))
        {
            allAvailable = false;
            log.showLog("Hardware", "Relic recovery arm motor NOT available!!!");
        }

        if(allAvailable)
        {
            log.showLog("Hardware", "Available!");
        }

        return allAvailable;
    }

    /**
     * The powerMotor() method sets power to a given motor. It translates the power value [-1,+1]
     * into a from the motor understandable set of instructions. Only stressing the motor without
     * it being able to move (because of too less power) is prevented by this method.
     *
     * @param motor The motor which will be powered (moved)
     * @param power The power value to power the motor with (from -1 until +1, 0=stop)
     */
    private void powerMotor(DcMotor motor, float power)
    {
        if(!isAvailable(motor)) //Just return if motor not available
        {
            return;
        }

        if (power < 0.1 && power > 0)
        {
            power = (float) 0.1;
        }else if (power > -0.1 && power < 0)
        {
            power = (float) -0.1;
        }

        motor.setDirection(power >= 0 ? DcMotorSimple.Direction.FORWARD : DcMotorSimple.Direction.REVERSE);
        motor.setPower(power >= 0 ? power : -power);
    }

    /**
     * The powerServo() method sets power to a given servo. It translates the power value into a
     * from the servo understandable set of instructions. For servos the power value can out of the
     * range of -1 until +1 enabling the servo to move faster, because setting the power to 1 is
     * equal to moving the servo by 1 degree for each time this function is called. So In order to
     * have the servo continually moving this method has to be repeatedly called.
     *
     * @param servo The servo which will be powered (moved)
     * @param power The power value to power the servo with (can be out of the range from -1 to +1)
     */
    private void powerServo(Servo servo, double power)
    {
        if(!isAvailable(servo)) //Just return if servo not available
        {
            return;
        }

        power /= 360;
        //servo.setDirection(power >= 0 ? Servo.Direction.FORWARD : Servo.Direction.REVERSE);

        double currentPosition = servo.getPosition();
        double newPosition = currentPosition + power;

        if(newPosition < 0 || newPosition > 1)
        {
            newPosition = currentPosition;
        }

        servo.setPosition(newPosition);
    }
}
