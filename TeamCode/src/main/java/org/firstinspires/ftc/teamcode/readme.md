# 7175 Walbots Code (TeamCode module)


## Important Notice

Hi, we (the Walbots) are a registered FTC team competing in the First Tech Challenge "Relic Recovery."
The competitions take place from fall 2017 until summer 2018 and it is prohibited to copy any our
code during the season without written approval. After the competition season has ended in Fall 2018
the TeamCode module is open source and you can use it in it's original or modified version.

For the whole TeamCode module the following is valid:

 * Copyright (c) ©2017-2018 David Friedland
 * Copyright (c) ©2017-2018 Jan Feng Cui
 * Copyright (c) ©2017-2018 Jordan Randleman
 * Copyright (c) ©2017-2018 Navi Transaraviput
 * Copyright (c) ©2017-2018 Nicolas Hohaus
 * Copyright (c) ©2017-2018 Walbots (7175)
 *
 * Resource: https://gitlab.com/roboticsclub/nico/competition_bot
 * Contact: nico@walbots.com, team@walbots.com



## Code Structure

We thought long and hard about how to structure our code in a solid and stable way.
This has led us to introduce multiple small classes and topics that we will discuss in detail
in the following sections.



## Autonomous

For the autonomous mode, we thought that we would have an improved usability if we can run
a list of pre-programmed actions step by step. An action is defined as an implementation of the class
AutonomousFunction and has a setup(), run() and shutdown() method. Instances of those actions can
be given to the class AutonomousController which starts the first action and once it finishes the
next one and so on. The controller also shows logging feedback to the drivers on the phone.

With this setup, we only have to instantiate the actions we want to run during autonomous and give
them to an AutonomousController object that handles and runs them. It is very easy to add, remove or
modify an action for the autonomous modes resulting in a more structured and robust implementation of the autonomous mode.
Aditionally this allows us to adopt new features and make modifications with a time constraint (We were able to develop an autonomous function that
the successfully drives the robot into the safe zone during a 10-minute break between two matches).



## User-Controlled

In contrast to the autonomous period, during the user-controlled driving period the robot does not
have to run through a pre-programmed set of actions. So we have come up with another way of making our code well structured and robust.

We thought for the user-controlled driving period it would be best if we divide the robot
into groups of actions that the drivers can perform. Each group has a Controller class that has
a run() method or an equivalent where the controller checks the gamepad inputs and reacts
accordingly. So every controller only has to look at the gamepad keys its actions are based on and
run them.

With this setup, we have a clean division of where we handle specific robot actions like driving,
moving an arm, opening/closing the grabber, etc. This makes it very easy for us to find bugs and make modifications
but also simplifies complicated code by not confusing it with
other actions in the same file.



## Real-Time Smart Vision

Besides efficiency and real-time management, our goal is to use computer vision to aid in the development of an intelligent robot.
We heard of Vuforia as an option for smart vision, but we decided to use the open source and
free to use computer vision library OpenCV instead for developing real-time smart vision. This
decision is based on external recommendations and an internal desire to develop our own analyzation
algorithms with editable access to the whole library.

We created various classes and an interface in the file OpenCV.java where we developed a structure
for an OpenCV image analyzation and OpenCV-TeamCode communication abilities. The OpenCV image
analyzation runs in a high-priority background thread.



## Additional
In addition to structuring the code for autonomous and user-controlled period, we also added
additional functionality making outstanding actions easier to use or handle.

-   For example, we developed the class LoggingController handling all our logs in a simple and easy
    way.
-   We also organized all the robot's hardware making it easily accessible in the class
    BasicHardwareController.
-   The PowerAdjustController class adjusts an input power (for example 0.5 or 1) into a
    natural value that enables improved handling of the robot.
-   Throughout the whole code we implemented multi-threading for improved real-time problem-solving.



## Our Code in detail


Note: The word "action" is often used when we talk about the autonomous period and an
AutonomousFunction class implementation. "Action" is something the robot does (driving,
moving, sensing etc) and is also used in this context with the user-controlled period.


### AutonomousFunction Class

The AutonomousFunction class is an abstract definition for implementing actions that can be run
by the AutonomousController during the autonomous period. It defines an action life-cycle starting
with setup(), then repeatedly calling run() until the action is completed and finally finishing with
the shutdown() method.

For every action that should be available to run during autonomous (eg drivingByTime, moveArm,
readColorSensor etc) a new class extending the AutonomousFunction class has to be created. In the
Walbots implementation all classes are named Autonomous_*****.

Through this implementation it is very easy for us to repeat the same action in slightly different
ways since we can instantiate multiple objects of the same action class. When we instantiate a new
object of an action, we can just initialize it with different values (eg driving for 1.0, 0.75, 3.0,
2.1.... seconds). With this, we do not have to program the same action more than once giving us more
reliability (fewer bugs) and stability.


### AutonomousController Class

The AutonomousController class is a controller developed for handling a set of AutonomousFunction
objects. Once it's runAllFunctions() method is called, it iterates through all given actions and
calls their setup(), run() and shutdown() methods.

With the AutonomousController it is very easy for us to write autonomous modes. The only thing we
have to do is create objects of actions we want to run in the autonomous mode and give all of them
to an AutonomousController object that handles them.


### AutonomousMode Class

The AutonomousMode class extends NewOpMode(OpMode) and is our abstract implementation of an
AutonomousMode. The only reason it is abstract is the four different start positions on
competitions giving us unknown variables. Therefore we defined the abstract methods isBlueAlliance()
and isFrontPlace() that have to be implemented by a subclass (in our case AutonomousMode_****) that
will be registered as an autonomous mode to display on the phone.

Basically, the AutonomousMode handles the whole autonomous period by creating objects of
actions(AutonomousFunction subclasses) and running them with the AutonomousController. It checks
it's abstract methods isBlueAlliance() and isFrontPlace() and creates little variations in the
behavior of the actions it runs. So we only have to create four subclasses for the four starting
positions, register them on the phone and return either true or false for the abstract methods.


### DrivingController, ClawController and KnockOffController Classes

We divided the robot into groups of actions. Each group has a controller that handles those actions
during the user-controlled period. Currently, we have four groups and four controllers.

Driving:        The driving controller checks the first gamepad's input keys and controls the four
                wheels moving the robot through the game field.

The Claw:       The claw controller checks the second gamepad's input keys and controls the grabber
                (claw) and the linear slide moving the grabber up and down.

Knock Off Arm:  The knock-off arm controller checks the second gamepad's emergency (not easy to
                access) input keys and moves the knock-off arm and prints the color sensor value on
                the phone. This controller is only programmed for development reasons and an
                emergency access to the knock-off arm during the competition.
                
Relic Arm:      The relic arm controller checks the second gamepad's input keys and controls the relic grabber
                and the extensions/contraction of the arm.


### CompetitionBotDrivingOP Class

The CompetitionBotDrivingOp class is our implementation for the registered user-controlled mode that
can be run on the phone. Instead of enabling the whole functionality here, we call all the
controllers from the previous section.


### NewOpMode Class

The NewOpMode class is abstract extending the OpMode class and building the structure for all our
intern OpMode extensions. We will extend NewOpMode instead of OpMode for creating user controlled
and autonomous modes.

The NewOpMode has a property hardware of the type BasicHardwareController giving access to the
robot's hardware. It also initializes the hardware so that we do not have to worry about this in
each OpMode implementation.


### PowerAdjustController

The PowerAdjustController class is developed for enhanced robot handling. It is used to adjust the
power value during the user-controlled period pushing the limits of control.
It enables the driver to drive the robot precisely in one situation, but with all energy
pushing the limits in another. The controller is based on various linear and quadratic functions
that can be influenced by pressing specific gamepad keys.


### BasicHardwareController Class

The BasicHardwareController class is our low-level implementation for controlling the robot's
hardware. The hardware itself is defined as private so that it can not be accessed from
outside. We think this is necessary to ensure that the hardware is always used in the right way.
For powering motors and reading sensor data and many other interactions with the hardware, we have
created public methods that can be called. Those methods deal with all the specialties of different
kinds of hardware and are also there for converting input values into instructions that the hardware can interpret.

With this, we do not have to worry about the exact functionality of a motor everywhere, or even the
differences between different motors. The methods enable us to set a negative power for
turning the motor in the other direction, and we also define the right direction to be forward
for motors here. This is where hardware specific code takes place, that is only dependent on
the specific piece of hardware.

The hardware controller turned out to be the basis for a functioning robot. If the hardware
instructions don't work here, they don't work anywhere in the code. But what makes it so unique is that
we can enhance instructions and make them robot (and hardware) specific. If we want a piece of
hardware to work differently, we can just modify its control code to our maximum needs. And we
never have to worry about that piece of hardware in any of our autonomous or user-controlled modes,
because there we just call the public methods.


### LoggingController Class

The LoggingController class handles showing all our logs on the phone (newest ones first).
After a new log is added, it will automatically update the phone and show it on the top. By having
the LoggingController available everywhere anytime, its current instance can be accessed
through it's static method getCurrentInstance(). In this way we do not have to pass the controller
from object to object, they can just get it directly.

Having a controller managing the logs has made it easier for us to give direct feedback to the user.
Normally the new log would add on on the bottom, but with this, we also enhance the user experience.
The user does not have to scroll all the way down to see the most recent log because we display it
on the top of all other logs.



# FTC (First Tech Challenge) TeamCode Module Setup Guide


## TeamCode Module

Welcome!

This module, TeamCode, is the place where you will write/paste the code for your team's
robot controller App. This module is currently empty (a clean slate) but the
process for adding OpModes is straightforward.

## Creating your own OpModes

The easiest way to create your own OpMode is to copy a Sample OpMode and make it your own.

Sample opmodes exist in the FtcRobotController module.
To locate these samples, find the FtcRobotController module in the "Project/Android" tab.

Expand the following tree elements:
 FtcRobotController / java / org.firstinspires.ftc.robotcontroller / external / samples

A range of different samples classes can be seen in this folder.
The class names follow a naming convention which indicates the purpose of each class.
The full description of this convention is found in the samples/sample_convention.md file.

A brief synopsis of the naming convention is given here:
The prefix of the name will be one of the following:

* Basic:    This is a minimally functional OpMode used to illustrate the skeleton/structure
            of a particular style of OpMode.  These are bare bones examples.
* Sensor:   This is a Sample OpMode that shows how to use a specific sensor.
            It is not intended as a functioning robot, it is simply showing the minimal code
            required to read and display the sensor values.
* Hardware: This is not an actual OpMode, but a helper class that is used to describe
            one particular robot's hardware devices: eg: for a Pushbot.  Look at any
            Pushbot sample to see how this can be used in an OpMode.
            Teams can copy one of these to create their own robot definition.
* Pushbot:  This is a Sample OpMode that uses the Pushbot robot structure as a base.
* Concept:	This is a sample OpMode that illustrates performing a specific function or concept.
            These may be complex, but their operation should be explained clearly in the comments,
            or the header should reference an external doc, guide or tutorial.
* Library:  This is a class, or set of classes used to implement some strategy.
            These will typically NOT implement a full OpMode.  Instead they will be included
            by an OpMode to provide some stand-alone capability.

Once you are familiar with the range of samples available, you can choose one to be the
basis for your own robot.  In all cases, the desired sample(s) needs to be copied into
your TeamCode module to be used.

This is done inside Android Studio directly, using the following steps:

 1) Locate the desired sample class in the Project/Android tree.

 2) Right click on the sample class and select "Copy"

 3) Expand the  TeamCode / java folder

 4) Right click on the org.firstinspires.ftc.teamcode folder and select "Paste"

 5) You will be prompted for a class name for the copy.
    Choose something meaningful based on the purpose of this class.
    Start with a capital letter, and remember that there may be more similar classes later.

Once your copy has been created, you should prepare it for use on your robot.
This is done by adjusting the OpMode's name, and enabling it to be displayed on the
Driver Station's OpMode list.

Each OpMode sample class begins with several lines of code like the ones shown below:

```
 @TeleOp(name="Template: Linear OpMode", group="Linear Opmode")
 @Disabled
```

The name that will appear on the driver station's "opmode list" is defined by the code:
 ``name="Template: Linear OpMode"``
You can change what appears between the quotes to better describe your opmode.
The "group=" portion of the code can be used to help organize your list of OpModes.

As shown, the current OpMode will NOT appear on the driver station's OpMode list because of the
  ``@Disabled`` annotation which has been included.
This line can simply be deleted , or commented out, to make the OpMode visible.



## ADVANCED Multi-Team App management:  Cloning the TeamCode Module

In some situations, you have multiple teams in your club and you want them to all share
a common code organization, with each being able to *see* the others code but each having
their own team module with their own code that they maintain themselves.

In this situation, you might wish to clone the TeamCode module, once for each of these teams.
Each of the clones would then appear along side each other in the Android Studio module list,
together with the FtcRobotController module (and the original TeamCode module).

Selective Team phones can then be programmed by selecting the desired Module from the pulldown list
prior to clicking to the green Run arrow.

Warning:  This is not for the inexperienced Software developer.
You will need to be comfortable with File manipulations and managing Android Studio Modules.
These changes are performed OUTSIDE of Android Studios, so close Android Studios before you do this.
 
Also.. Make a full project backup before you start this :)

To clone TeamCode, do the following:

Note: Some names start with "Team" and others start with "team".  This is intentional.

1)  Using your operating system file management tools, copy the whole "TeamCode"
    folder to a sibling folder with a corresponding new name, eg: "Team0417".

2)  In the new Team0417 folder, delete the TeamCode.iml file.

3)  the new Team0417 folder, rename the "src/main/java/org/firstinspires/ftc/teamcode" folder
    to a matching name with a lowercase 'team' eg:  "team0417".

4)  In the new Team0417/src/main folder, edit the "AndroidManifest.xml" file, change the line that contains
         package="org.firstinspires.ftc.teamcode"
    to be
         package="org.firstinspires.ftc.team0417"

5)  Add:    include ':Team0417' to the "/settings.gradle" file.
    
6)  Open up Android Studios and clean out any old files by using the menu to "Build/Clean Project""