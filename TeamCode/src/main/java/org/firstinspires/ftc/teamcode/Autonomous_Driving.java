package org.firstinspires.ftc.teamcode;

/**
 * Created by nicolas on 11/18/17 in competition_bot.
 *
 * Copyright (c) ©2017 Nicolas Hohaus
 * Copyright (c) ©2017 Walbots (7175)
 *
 * Resource: https://gitlab.com/roboticsclub/nico/competition_bot
 * Contact: nico@walbots.com, team@walbots.com
 */


/**
 * The class Autonomous_Driving is an AutonomousFunction that handles the driving of the robot.
 *
 * The speed of the wheels is defined by a vector4 and the duration of driving is dependent to
 * the property timeToWaitAfterFinished.
 */
public class Autonomous_Driving extends AutonomousFunction
{

    private float[] vector4DrivingPower;//The vector4 for the power of all wheels (bl, br, fl, fr)

    /**
     * Initializes an instance of Autonomous_Driving
     *
     * @param hardware The hardware controller object of the robot to access and control it's hardware
     * @param name The name of the instance of the AutonomousFunction (will be displayed on the phone)
     * @param vector4DrivingPower vector with 4 floats (backLeft, backRight, frontLeft, frontRight)
     */
    public Autonomous_Driving(BasicHardwareController hardware, String name, float[] vector4DrivingPower)
    {
        super(hardware, name);
        this.vector4DrivingPower = vector4DrivingPower;
    }

    /**
     * The setup() method is called by the AutonomousController for setting up the
     * AutonomousFunction preparing it for the run() method
     *
     * In this case there is nothing that has to be set up
     */
    @Override
    public void setup()
    {
        //Nothing to set up
    }

    /**
     * The run() method is called continually by the AutonomousController until the
     * function returns FINISHED (runs as long as it returns RUNNING)
     *
     * This method will set the power to all of the wheels and directly return FINISHED
     *
     * @return RUNNING, FINISHED, (nil=error)
     */
    @Override
    public FunctionState run()
    {
        //Start driving
        hardware.powerBackMotorLeft(vector4DrivingPower[0]);
        hardware.powerBackMotorRight(vector4DrivingPower[1]);
        hardware.powerFrontMotorLeft(vector4DrivingPower[2]);
        hardware.powerFrontMotorRight(vector4DrivingPower[3]);

        return FunctionState.FINISHED;
    }

    /**
     * The shutdown() method is called by the AutonomousController for finishing up all tasks and
     * getting to a state where the AutonomousFunction is completed.
     *
     * In this case the motors moving the wheels are stopped so that the robot stops at the
     * destination.
     */
    @Override
    public void shutdown()
    {
        //Stop driving
        hardware.powerBackMotorLeft(0);
        hardware.powerBackMotorRight(0);
        hardware.powerFrontMotorLeft(0);
        hardware.powerFrontMotorRight(0);
    }
}
