package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;

import java.security.KeyStore;

/**
 * Created by nicolas on 10/19/17 in competition_bot.
 *
 * Copyright (c) ©2017 Nicolas Hohaus
 * Copyright (c) ©2017 Walbots (7175)
 *
 * Resource: https://gitlab.com/roboticsclub/nico/competition_bot
 * Contact: nico@walbots.com, team@walbots.com
 */


/**
 * The abstract class NewOpMode defines the basic structure for all OpMode implementations in this
 * project (Autonomous & UserControlled) and handles the robot's hardware.
 *
 * (Not OpMode will by extended in this project, NewOpMode or it's implementations will be extended
 * instead)
 */
public abstract class NewOpMode extends OpMode
{
    /**
     * The hardware property gives NewOpModes access to the hardware of the robot
     */
    public BasicHardwareController hardware;

    /**
     * The abstract init() method is a redefinition of the init() method of the OpMode. It is called
     * by the NewOpMode once the init() method from OpMode gets called. This happens when the
     * initialize button is pressed on the phone.
     *
     * @param initialize This boolean will always be true, you can ignore it
     * @return Whether the OpMode initialized successfully
     */
    abstract public boolean init(boolean initialize);

    /**
     * Initializes a NewOpMode and with it the robot's hardware.
     */
    public NewOpMode()
    {
        super();
        new LoggingController(telemetry);
    }

    /**
     * Initializes the hardware so that the OpModes can use it
     */
    @Override
    public final void init()
    {
        //Initialize Hardware
        this.hardware = new BasicHardwareController(hardwareMap);

        //Call the init() method
        boolean successfullyInitialized = init(true);
        LoggingController.getCurrentInstance().showLog("NewOpMode", successfullyInitialized ? "Initialized" : "Failed to initialize!");
    }
}
