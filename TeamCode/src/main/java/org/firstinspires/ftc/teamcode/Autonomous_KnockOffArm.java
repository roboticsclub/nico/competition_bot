package org.firstinspires.ftc.teamcode;

/**
 * Created by nicolas on 11/18/17 in competition_bot.
 *
 * Copyright (c) ©2017 Nicolas Hohaus
 * Copyright (c) ©2017 Walbots (7175)
 *
 * Resource: https://gitlab.com/roboticsclub/nico/competition_bot
 * Contact: nico@walbots.com, team@walbots.com
 */


/**
 * The class Autonomous_KnockOffArm is an AutonomousFunction that handles the knock off arm to move
 * up and down to the right positions.
 */
public class Autonomous_KnockOffArm extends AutonomousFunction
{

    private boolean goDown; //Indicating whether the knock off arm should move up or down

    /**
     * Initializes and instance of Autonomous_KnockOffArm
     *
     * @param hardware The hardware controller object of the robot to access and control it's hardware
     * @param name The name of the instance of the AutonomousFunction (will be displayed on the phone)
     * @param goDown Indicating whether the knock off arm should move up or down
     */
    Autonomous_KnockOffArm(BasicHardwareController hardware, String name, boolean goDown)
    {
        super(hardware,name);

        this.goDown = goDown;
    }

    /**
     * The setup() method is called by the AutonomousController for setting up the
     * AutonomousFunction preparing it for the run() method
     *
     * In this case the knock off arm servo is initialized
     */
    @Override
    public void setup()
    {
        //Initialize the arm
        hardware.initializeKnockOffArmServo();
    }

    /**
     * The run() method is called continually by the AutonomousController until the
     * function returns FINISHED (runs as long as it returns RUNNING)
     *
     * This method will move the knock off arm either up or down to the right position
     *
     * @return RUNNING, FINISHED, (nil=error)
     */
    @Override
    public FunctionState run()
    {
        //Move the knock off arm
        if(goDown)
        {
            hardware.powerKnockOffArmServo(-225);
        }else
        {
            hardware.initializeKnockOffArmServo();
        }

        return FunctionState.FINISHED;
    }

    /**
     * The shutdown() method is called by the AutonomousController for finishing up all tasks and
     * getting to a state where the AutonomousFunction is completed.
     *
     * In this case everything is already finished once this method gets called and nothing is done.
     */
    @Override
    public void shutdown()
    {
        return;
    }
}
