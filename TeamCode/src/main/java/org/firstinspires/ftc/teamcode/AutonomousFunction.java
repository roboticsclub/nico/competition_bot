package org.firstinspires.ftc.teamcode;

/**
 * Created by nicolas on 11/7/17 in competition_bot.
 *
 * Copyright (c) ©2017 Nicolas Hohaus
 * Copyright (c) ©2017 Walbots (7175)
 *
 * Resource: https://gitlab.com/roboticsclub/nico/competition_bot
 * Contact: nico@walbots.com, team@walbots.com
 */


/**
 * The abstract class AutonomousFunction defines basic behaviour and functionality for
 * AutonomousFunctions. This provides a base for the AutonomousController to interact with various
 * types and different implementations (subclasses) of the AutonomousFunction class giving the
 * opportunity to create big sets of individual actions that have a common standardized definition
 * and can be completed during Autonomous Mode.
 *
 * The life-cycle with the AutonomousController is:
 * - initialize()
 * - setup()
 * - run()          (can be called more than once)
 * - shutdown()
 */
public abstract class AutonomousFunction
{
    /**
     * The hardware property gives AutonomousFunctions access to the hardware of the robot
     */
    BasicHardwareController hardware;

    /**
     * Instantiates an AutonomousFunction with the robot's hardware and a descriptive name
     *
     * @param hardware The hardware controller of the robot
     * @param name The name of the instance of the AutonomousFunction (will be displayed on the phone)
     */
    public AutonomousFunction(BasicHardwareController hardware, String name)
    {
        this.hardware = hardware;
        this.name = name;
    }

    /**
     * The FunctionState enum defines the various states an AutonomousFunction can be in
     */
    enum FunctionState
    {
        /**
         * The AutonomousFunction is running, so the run() method will be called (continuously)
         */
        RUNNING,
        /**
         * The AutonomousFunction is finished, so it is ready for shutdown()
         */
        FINISHED
    }


    /**
     * A descriptive name for the instance of the AutonomousFunction implementation that will be
     * displayed on the phone for information purposes
     */
    String name = "";

    /**
     * The amount of time that has to pass by in the life-cycle after the run() method returned
     * FINISHED and before the shutdown() method is called.
     */
    long timeToWaitAfterFinished = 0;

    private long timeWhenFunctionStarted;

    /**
     * The abstract setup() method is overwritten by each implementation of the
     * AutonomousFunction. It is called by the AutonomousController for setting the
     * AutonomousFunction up and making it ready for the run() method
     */
    abstract public void setup();

    /**
     * The abstract run() method is overwritten by each implementation of the
     * AutonomousFunction. It is continually called by the AutonomousController as long as it
     * returns RUNNING and until it returns FINISHED.
     *
     * In the implementation of this method the specific autonomous action is done. Once the action
     * is finished, the function returns FINISHED.
     *
     * @return RUNNING, FINISHED, (nil=error(=FINISHED))
     */
    abstract public FunctionState run();

    /**
     * The abstract shutdown() method is overwritten by each implementation of the
     * AutonomousFunction. It is called by the AutonomousController after the run() method is
     * finished and the timeToWaitAfterFinished has passed by.
     *
     * In the implementation of this method the autonomous action is cleaning up (stopping motors or
     * cleaning up and finishing up if needed)
     */
    abstract public void shutdown();

    /**
     * The initialize() method is called by the AutonomousController to start the life-cycle of the
     * AutonomousFunction.
     *
     * The abstract setup() method is called and the current time is saved so that we know later for
     * how long a specific autonomous function is running.
     */
    final void initialize()
    {
        setup();
        timeWhenFunctionStarted = System.currentTimeMillis();
    }

    /**
     * The getRuntime() method calculates the time that passed since the setup() method was called
     * in the life-cycle of the AutonomousFunction until the current point of time.
     *
     * @return The time (ms) for how long the AutonomousFunction is running (since setup() finished)
     */
    final long getRuntime()
    {
        return System.currentTimeMillis() - timeWhenFunctionStarted;
    }
}
