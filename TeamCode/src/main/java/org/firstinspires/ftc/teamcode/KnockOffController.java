package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.Gamepad;

/**
 * Created by nicolas on 11/15/17 in competition_bot.
 *
 * Copyright (c) ©2017 Nicolas Hohaus
 * Copyright (c) ©2017 Walbots (7175)
 *
 * Resource: https://gitlab.com/roboticsclub/nico/competition_bot
 * Contact: nico@walbots.com, team@walbots.com
 */


/**
 * The class KnockOffController is enabling the UserControlled OpMode to move the knock off arm.
 * This is implemented as reactions to the input of the second gamepad defined by the OpMode.
 *
 * dpad_up (cross up):      Moving the knock off arm up. This is intended to be only for test and
 *                          emergency purposes. While moving the knock off arm, the color that is
 *                          read from the color sensor will be displayed on the phone.
 *
 * dpad_down (cross down):  Moving the knock off arm down. This is intended to be only for test and
 *                          emergency purposes. While moving the knock off arm, the color that is
 *                          read from the color sensor will be displayed on the phone.
 */
public class KnockOffController
{
    private BasicHardwareController hardware;   //The hardware to access components of the robot

    private Gamepad gamepad2;                   //The gamepad for user input

    /**
     * Initializing a KnockOffController with a NewOpMode that is currently running and that's
     * functionality should be extended by this controller.
     *
     * @param currentOpMode The NewOpMode that is currently running
     */
    public KnockOffController(NewOpMode currentOpMode)
    {
        hardware = currentOpMode.hardware;

        gamepad2 = currentOpMode.gamepad2;

        hardware.initializeKnockOffArmServo();
    }

    /**
     * The run() method should be called by the current NewOpMode continually (every time when it's
     * loop() method gets called) so that this controller can read gamepad input data and let the
     * robot react with specified actions.
     *
     * In this case the knock off arm will be moved up and down and the color sensor value will be
     * printed on the phone while doing so.
     *
     * dpad_up (cross up):      Moving the knock off arm up. This is intended to be only for test and
     *                          emergency purposes. While moving the knock off arm, the color that is
     *                          read from the color sensor will be displayed on the phone.
     *
     * dpad_down (cross down):  Moving the knock off arm down. This is intended to be only for test and
     *                          emergency purposes. While moving the knock off arm, the color that is
     *                          read from the color sensor will be displayed on the phone.
     */
    public void run()
    {

        if (gamepad2 == null)
        {
            return; //If gamepad is not connected, return function
        }else if (gamepad2.dpad_up)
        {
            hardware.powerKnockOffArmServo(5);
            showColorOnDriverStation();
        }else if(gamepad2.dpad_down)
        {
            hardware.powerKnockOffArmServo(-5);
            showColorOnDriverStation();
        }
    }

    /**
     * The showColorOnDriverStation() method will print the current color value of the color sensor
     * on the phone. Before reading the color value the LED on the color sensor will be turned on
     * and afterwards turned off.
     */
    private void showColorOnDriverStation()
    {
        hardware.enableKnockOffColorSensorLED(true);
        LoggingController.getCurrentInstance().showLog("Color Detected", "Time: %f Blue: %d, Red: %d", (float)System.currentTimeMillis(), hardware.getKnockOffColorSensorBlue(), hardware.getKnockOffColorSensorRed());
        hardware.enableKnockOffColorSensorLED(false);
    }
}
