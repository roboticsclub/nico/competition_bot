package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.Gamepad;

/**
 * Created by nicolas on 10/19/17 in competition_bot.
 *
 * Copyright (c) ©2017 Nicolas Hohaus
 * Copyright (c) ©2017 Walbots (7175)
 *
 * Resource: https://gitlab.com/roboticsclub/nico/competition_bot
 * Contact: nico@walbots.com, team@walbots.com
 */


/**
 * The class PowerAdjustController is designed to adjust the power value that is given to motors.
 * This adjustment is based one some gamepad keys and some predefined factors that make the output
 * more convenient and natural.
 *
 * - Left_Trigger:  Adjusting the power to be always slower than the original enabling the robot
 *                  to be controlled very precisely
 *
 * - Right_Trigger: Adding up to 0.5 to the power value enabling to reach a maximum power of 1
 *                  if the robot needs to drive faster or motors need more power to complete a
 *                  specific task.
 */
public class PowerAdjustController
{
    private Gamepad gamepad;    //The gamepad where the keys for the adjustments can be pressed
    private float power = 0;    //The power value to adjust

    /**
     * Instantiates a PowerAdjustController with the gamepad, on which the adjustment keys can be
     * pressed to control specific adjustment factors.
     *
     * @param gamepad The gamepad with keys that are considered for the adjustment factors
     */
    public PowerAdjustController(Gamepad gamepad)
    {
        this.gamepad = gamepad;
    }

    /**
     * The getAdjustedPower methods takes a given power and adjusts it's value by some predefined
     * factors but also considering the gamepad input from some special keys.
     *
     * - Left_Trigger:  Adjusting the power to be always slower than the original enabling the robot
     *                  to be controlled very precisely
     *
     * - Right_Trigger: Adding up to 0.5 to the power value enabling to reach a maximum power of 1
     *                  if the robot needs to drive faster or motors need more power to complete a
     *                  specific task.
     *
     * @param power The original power value
     * @return The adjusted more convenient and natural power value
     */
    public float getAdjustedPower(float power)
    {
        this.power = power;

        boolean isPowerNegative = power < 0;

        adjustPower();
        adjustPowerByGamepadKey();


        if (isPowerNegative)
        {
            power = -power;
        }

        return this.power;
    }

    /**
     * The adjustPower() method makes sure that we are using only half of the possible power by
     * default so that the robot is moving in a controllable pace. (The full power can still be
     * reached by pressing the right trigger all the way down)
     */
    private void adjustPower()
    {
        power /= 2;
    }

    /**
     * The adjustPowerByGamepadKey() method adjusts the power value dependent on the input of the
     * right and left trigger. The left trigger has a quadratic function making the robot slower and
     * the right trigger has a linear function making the robot faster. This enables the drivers to
     * control the robot very precisely and reliably.
     */
    private void adjustPowerByGamepadKey()
    {
        float leftTriggerPowerOffset = (float) (0.15 + (gamepad.left_trigger * gamepad.left_trigger) / 3);
        float rightTriggerPowerOffset = gamepad.right_trigger / 2;

        if (power < 0)
        {
            power += leftTriggerPowerOffset; //make it slower
            power -= rightTriggerPowerOffset; //make it faster

            power = power > -0.1 ? (float) -0.1 : power;
        }else if (power > 0)
        {
            power -= leftTriggerPowerOffset; //make it slower
            power += rightTriggerPowerOffset; //make it faster

            power = power < 0.1 ? (float) 0.1 : power;
        }
    }
}
