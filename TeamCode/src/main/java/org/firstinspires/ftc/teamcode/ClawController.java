package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.Gamepad;

/**
 * Created by nicolas on 10/19/17 in competition_bot.
 *
 * Copyright (c) ©2017 Nicolas Hohaus
 * Copyright (c) ©2017 Walbots (7175)
 *
 * Resource: https://gitlab.com/roboticsclub/nico/competition_bot
 * Contact: nico@walbots.com, team@walbots.com
 */


/**
 * The class ClawController is enabling the UserControlled OpMode to move the linear slide and the
 * grabbers (claw). This is implemented as reactions to the input of the second gamepad defined by
 * the OpMode.
 *
 * Left_Stick (y-axis): Moving the linear slide respectively up or down
 *
 * Left_Trigger:        Closing the top grabber (claws)
 *
 * Left_Bumper:         Opening the top grabber (claws)
 *
 * Right_Trigger:       Closing the bottom grabber (claws)
 *
 * Right_Bumper:        Opening the bottom grabber (claws)
 */
public class ClawController
{
    private BasicHardwareController hardware;   //The hardware to access components of the robot

    private Gamepad gamepad2;                   //The gamepad for user input

    /**
     * Initializing a ClawController with a NewOpMode that is currently running and that's
     * functionality should be extended by this controller.
     *
     * @param currentOpMode The NewOpMode that is currently running
     */
    public ClawController(NewOpMode currentOpMode)
    {
        hardware = currentOpMode.hardware;

        gamepad2 = currentOpMode.gamepad2;

        hardware.initialiseTopGrabberServos();
        hardware.initialiseBottomGrabberServos();
    }

    /**
     * The run() method should be called by the current NewOpMode continually (every time when its
     * loop() method gets called) so that this controller can read gamepad input data and let the
     * robot react with specified actions.
     *
     * In this case the claws will be opened/closed and the linear slide will be moved up or down.
     *
     * Left_Stick (y-axis): Moving the linear slide respectively up or down
     *
     * Left_Trigger:        Closing the top grabber (claws)
     *
     * Left_Bumper:         Opening the top grabber (claws)
     *
     * Right_Trigger:       Closing the bottom grabber (claws)
     *
     * Right_Bumper:        Opening the bottom grabber (claws)
     */
    public void run()
    {
        if (gamepad2 == null)
        {
            return; //If gamepad is not connected, return function
        }

        moveLinearSlide(gamepad2.left_stick_y);

        float topClawPower = 0;
        float bottomClawPower = 0;

        if(gamepad2.left_bumper ^ gamepad2.left_trigger != 0)//^ is the XOR operator
        {
            topClawPower = gamepad2.left_bumper ? -5 : gamepad2.left_trigger * 5;
        }

        if (gamepad2.right_bumper ^ gamepad2.right_trigger != 0)//^is the XOR operator
        {
            bottomClawPower = gamepad2.right_bumper ? -5 : gamepad2.right_trigger * 5;
        }

        moveTopClaw(topClawPower);
        moveBottomClaw(bottomClawPower);
    }


    /**
     * The moveLinearSlide() method moves the linear slide on which the grabbers are mounted up and
     * down.
     *
     * @param power Power value moving the motor (0=stop)
     */
    private void moveLinearSlide(float power)
    {
        hardware.powerLinearSlideMotors(power);
    }

    /**
     * The moveTopClaw() method moves the top grabber planes (claw) with a given power. In order to
     * keep the grabber (claws) moving this function has to be called continuously.
     *
     * @param power Power value moving the servos (0=no movement)
     */
    private void moveTopClaw(double power)
    {
        if(power == 0)
        {
            return; //Return since servos automatically stop when they arrive at destination
        }

        hardware.powerTopGrabberServoLeft(power);
        hardware.powerTopGrabberServoRight(power);
    }

    /**
     * The moveBottomClaw() method moves the bottom grabber planes (claw) with a given power. In
     * order to keep the grabber (claws) moving this function has to be called continuously.
     *
     * @param power Power value moving the servos (0=no movement)
     */
    private void moveBottomClaw(double power)
    {
        if(power == 0)
        {
            return; //Return since servos automatically stop when they arrive at destination
        }

        hardware.powerBottomGrabberServoLeft(power);
        hardware.powerBottomGrabberServoRight(power);
    }
}
