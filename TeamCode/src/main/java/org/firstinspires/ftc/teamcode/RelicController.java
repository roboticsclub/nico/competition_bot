package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.Gamepad;

import javax.net.ssl.HandshakeCompletedEvent;


/**
 * Created by nicolas on 1/12/18 in competition_bot.
 * <p>
 * Copyright (c) ©2018 Nicolas Hohaus
 * Copyright (c) ©2018 Walbots (7175)
 * <p>
 * Resource: https://gitlab.com/roboticsclub/nico/competition_bot
 * Contact: nico@walbots.com, team@walbots.com
 */


/**
 * The class RelicController is enabling the UserControlled OpMode to move the relic recovery arm
 * the grabber. This is implemented as reactions to the input of the second gamepad defined by the
 * OpMode.
 *
 * Right_Stick (y-axis):    Extending/Contracting the relic recovery arm
 *
 * Right_Stick_Button:      Opening the relic grabber fast
 *
 * Dpad_Left:               Opening the relic grabber
 *
 * Dpad_Right:              Closing the relic grabber
 */
public class RelicController
{
    private BasicHardwareController hardware;   //The hardware to access components of the robot

    private Gamepad gamepad2;                   //The gamepad for user input

    /**
     * Initializing a RelicController with a NewOpMode that is currently running and that's
     * functionality should be extended by this controller.
     *
     * @param currentOpMode The NewOpMode that is currently running.
     */
    public RelicController(NewOpMode currentOpMode)
    {
        hardware = currentOpMode.hardware;

        gamepad2 = currentOpMode.gamepad2;

        hardware.initializeRelicRecoveryGrabberServo();
    }

    /**
     * The run() method should be called by the current NewOpMode continually (every time when its
     * loop() method gets called) so that this controller can read gamepad input data and let the
     * robot react with specified actions.
     *
     * In this case the relic recovery arm will be extended/contracted and the grabber will be
     * opened or closed.
     *
     * Right_Stick (y-axis):    Extending/Contracting the relic recovery arm
     *
     * Right_Stick_Button:      Opening the relic grabber fast
     *
     * Dpad_Left:               Opening the relic grabber
     *
     * Dpad_Right:              Closing the relic grabber
     */
    public void run()
    {
        if (gamepad2 == null)
        {
            return; //If gamepad is not connected, return function
        }

        if(gamepad2.right_stick_button)
        {
            moveRelicRecoveryGrabber(3);
            moveRelicRecoveryArm(0);    //Stop arm when moving grabber

        }else
        {
            moveRelicRecoveryArm(-gamepad2.right_stick_y/3);
        }

        if(gamepad2.dpad_right)
        {
            moveRelicRecoveryGrabber(-1);

        }else if(gamepad2.dpad_left)
        {
            moveRelicRecoveryGrabber(1);
        }
    }

    /**
     * The moveRelicRecoveryArm() method extends/contracts the relic recovery arm with the grabber
     * mounted at its end.
     *
     * @param power Power value extending (positive) or contracting (negative) the arm (0=no movement)
     */
    private void moveRelicRecoveryArm(float power)
    {
        hardware.powerRelicRecoveryArmMotor(power);
    }

    /**
     * The moveRelicRecoveryGrabber() method opens and closes the relic recovery grabber.
     *
     * @param power Power value moving the servos (+=open) (-=close) (0=no movement)
     */
    private void moveRelicRecoveryGrabber(float power)
    {
        hardware.powerRelicRecoveryGrabberServo(power);
    }
}
