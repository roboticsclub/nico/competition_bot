package org.firstinspires.ftc.teamcode;


import com.qualcomm.robotcore.hardware.Gamepad;
import com.qualcomm.robotcore.hardware.HardwareMap;

/**
 * Created by nicolas on 10/16/17 in competition_bot.
 *
 * Copyright (c) ©2017 Nicolas Hohaus
 * Copyright (c) ©2017 Walbots (7175)
 *
 * Resource: https://gitlab.com/roboticsclub/nico/competition_bot
 * Contact: nico@walbots.com, team@walbots.com
 */


/**
 * The class DrivingController is enabling the UserControlled OpMode to drive the robot. This is
 * implemented as reactions to the input of the first gamepad defined by the OpMode.
 *
 * Left_Stick (xy-axis):    Driving the robot forward/backward and turning it to the right and left
 *
 * Right_Stick (x-axis):    Driving the robot sideways (strafing) to the right or left
 *
 * Left_Bumper:             Turning the robot on the point to the left
 *
 * Right_Bumper:            Turning the robot on the point to the right
 *
 * The driving speed is adjusted by the PowerAdjustController and respectively the right and left
 * trigger enabling a very precise driving of the robot.
 *
 * @see PowerAdjustController
 */
public class DrivingController
{
    BasicHardwareController hardware;   //The hardware to access components of the robot
    private Gamepad gamepad1;           //The gamepad for user input

    /**
     * Initializing a DrivingController with a NewOpMode that is currently running and that's
     * functionality should be extended by this controller.
     *
     * @param currentOpMode The NewOpMode that is currently running
     */
    public DrivingController(NewOpMode currentOpMode)
    {
        hardware = currentOpMode.hardware;
        gamepad1 = currentOpMode.gamepad1;
    }

    /**
     * The drive() method should be called by the current NewOpMode continually (every time when it's
     * loop() method gets called) so that this controller can read gamepad input data and let the
     * robot react with specified actions.
     *
     * In this case the wheels will be turned in various ways moving the robot in the game field.
     *
     * Left_Stick (xy-axis):    Driving the robot forward/backward and turning it to the right and left
     *
     * Right_Stick (x-axis):    Driving the robot sideways (strafing) to the right or left
     *
     * Left_Bumper:             Turning the robot on the point to the left
     *
     * Right_Bumper:            Turning the robot on the point to the right
     *
     * The driving speed is adjusted by the PowerAdjustController and respectively the right and left
     * trigger enabling a very precise driving of the robot.
     *
     * @see PowerAdjustController
     */
    void drive()
    {
        if(gamepad1 == null)
        {
            return; //If gamepad is not connected, return this function
        }

        PowerAdjustController powerAdjustController = new PowerAdjustController(gamepad1);

        if(gamepad1.left_stick_y != 0 || gamepad1.left_stick_x != 0)
        {
            float power = powerAdjustController.getAdjustedPower(gamepad1.left_stick_y);
            float direction = powerAdjustController.getAdjustedPower(gamepad1.left_stick_x);

            driveForward(power, direction);

        }else if (gamepad1.right_stick_x != 0)
        {
            float power = powerAdjustController.getAdjustedPower(gamepad1.right_stick_x);

            driveSideway(power);

        }else if (gamepad1.left_bumper || gamepad1.right_bumper)
        {
            driveForward(0, (float) (gamepad1.right_bumper ? 0.5 : -0.5));
        }else
        {
            allStop();
        }
    }

    /**
     * The stop() method will stop the robot from driving. (Once the drive() method is called the
     * robot might continue to drive again)
     */
    void stop()
    {
        allStop();
    }

    /**
     * The driveForward() method translates the input power values forward and direction into from
     * the BasicHardwareController understandable sets of instructions making the robot drive in the
     * given way.
     *
     * @param power Number between -1 (backward) and 1 (Forward)
     * @param direction Number between -1 (left) and 1 (right)
     */
    private void driveForward(float power, float direction)
    {
        float rightOffset = -direction; //make right slower so it turns right on positive
        float leftOffset = direction; //make left faster so it turns right on positive

        float powerMotorsRight = power + rightOffset;
        float powerMotorsLeft = power + leftOffset;

        hardware.powerBackMotorLeft(powerMotorsLeft);
        hardware.powerBackMotorRight(powerMotorsRight);
        hardware.powerFrontMotorLeft(powerMotorsLeft);
        hardware.powerFrontMotorRight(powerMotorsRight);
    }

    /**
     * The driveSideway() method makes the robot drive sideways (strafing) with a given power.
     *
     * @param power Number between -1 (left) and 1 (right)
     */
    private void driveSideway(float power)
    {
        hardware.powerBackMotorLeft(power);
        hardware.powerBackMotorRight(-power);
        hardware.powerFrontMotorLeft(-power);
        hardware.powerFrontMotorRight(power);
    }

    /**
     * The allStop() method sets the power to 0 for all driving motors so that the robot will stop
     * driving.
     */
    private void allStop()
    {
        hardware.powerBackMotorLeft(0);
        hardware.powerBackMotorRight(0);
        hardware.powerFrontMotorLeft(0);
        hardware.powerFrontMotorRight(0);
    }
}
