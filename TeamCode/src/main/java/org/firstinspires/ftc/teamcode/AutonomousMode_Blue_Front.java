package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

/**
 * Created by nicolas on 12/2/17 in competition_bot.
 *
 * Copyright (c) ©2017 Nicolas Hohaus
 * Copyright (c) ©2017 Walbots (7175)
 *
 * Resource: https://gitlab.com/roboticsclub/nico/competition_bot
 * Contact: nico@walbots.com, team@walbots.com
 */


/**
 * The class AutonomousMode_Blue_Front implements AutonomousMode and is registered so that it will
 * show up on the phone. It is defined to be running when we are in the blue alliance on the front
 * starting position. All actions are done by it's super class AutonomousMode.
 *
 * @see org.firstinspires.ftc.teamcode.AutonomousMode
 */
@Autonomous(name = "Blue Alliance Front", group = "Autonomous")
public class AutonomousMode_Blue_Front extends AutonomousMode
{
    /**
     * The AutonomousMode implementation AutonomousMode_Blue_Front is defined to be in the blue
     * alliance so this function will always return true.
     *
     * @return Always true for being in the blue alliance.
     */
    @Override
    public boolean isBlueAlliance()
    {
        return true;
    }

    /**
     * The AutonomousMode implementation AutonomousMode_Blue_Front is defined to be in the front
     * place so this function will always return true.
     *
     * @return Always true for being in the front place.
     */
    @Override
    public boolean isFrontPlace()
    {
        return true;
    }
}
